"""
  Optogenetics and Neural Engineering Core ONE Core
  University of Colorado, School of Medicine
  31.Oct.2021
  See bit.ly/onecore for more information, including a more detailed write up.
  LJ_data_processing.py

  ##############################################################################
  This code is designed to take in one or more .dat files from a data storage
  folder (in our case, the files were created from a LabJack DAQ system),
  combine the data portions from these files, and then output a Numpy (.npy)
  and/or CSV file containing all of the data for futher data analysis. If the
  data files include headers, this code will also offer the option to either
  strip the header from the CSV output file, or re-attach it once the data has
  been combined.
  Please check all imported packages used to run this code, and install them
  if you do not already have them using "pip install <package-name> in your
  OS's command terminal.

  USER INPUTS EXPLAINED:
  = header_present = (bool) Use "True" if all .dat files have headers, or use
                     "False" if there are no headers. If some do and some don't,
                     then you need to separate the files into header/non-header
                     folders or manually remove the headers from the data.
  = headerdata_split = (int) The row number where the header section ends and
                       the actual data begins. If header_present = False, then
                       you can disregard this variable.
  = save_npy = (bool) If set to "True", the code will output a Numpy (.npy) file
               to your chosen directory. You will need a .npy file to run the
               "extraction_barcodes.py" code.
  = save_csv = (bool) If set to "True", the code will output a CSV (.csv) file to
               your chosen directory. This is useful for visualizing your output
               or use with other data analysis tools that use .csv files.
  = keep_header = (bool) Set to True if your .dat files have headers and you want
                  to keep that header in the .csv file. Note that in order for the
                  header to be attached to the data, you must have both "save_csv"
                  and "header_present" set to True; the Numpy output file will not
                  have the header either way, so keep that in mind.
  = out_file_name = (str) The name you wish to give the output file(s). Note that
                    the file will include the date and time when it was created
                    attached to the end of the filename (YYYY-MM-DD-HH-MM-SS)
  ################################################################################

  References

"""
#######################
### Imports Section ###
#######################

import os
import sys
import numpy as np
import re
from numpy import savetxt
from natsort import os_sorted
from datetime import datetime
from pathlib import Path
from tkinter.filedialog import askdirectory

################################################################################
############################ USER INPUT SECTION ################################
################################################################################

header_present = True # Set to True if all .dat files have headers.
headerdata_split = 10 # Row number where data separates from header.
save_npy = True  # Set to True if you want to save the output as a .npy file
save_csv = True  # Set to True if you want to save the output as a .csv file
keep_header = False # Set to True if you want the header put back in the csv file
out_file_name = 'SugarWaterTest_9Nov2021' # Name of output file

################################################################################
############################ END OF USER INPUT #################################
################################################################################

##############################################################
### Select Raw Data Directory / Processed Output Directory ###
##############################################################

try:
    lj_in_data_dir = Path(askdirectory(title = 'Select Raw Input Data Folder', ))
    print("In Directory: ", lj_in_data_dir)
except:
    print("No Input Directory Chosen")
    sys.exit()

if save_npy or save_csv:
    try:
        lj_out_data_dir = Path(askdirectory(title = 'Select Processed Output Folder', ))
    except:
        print("No Output Directory Chosen")
        sys.exit()
else:
    print("No outputs selected. What are you trying to do?")


lj_mat = list()
lj_array = np.empty(0) # Create empty array for storing numpy arrays if using save_npy
first_array = True        # Count arrays to use np.concatenate() after first array stored.

#########################################################################
### Collect .dat Files in Input Directory, Remove Headers and Combine ###
#########################################################################

file_name_list = os_sorted(os.listdir(lj_in_data_dir))

for file_name in file_name_list:
    if file_name[-3:] == 'dat':      # Only run on .dat files output from the LJ
        file_name_list_dir = lj_in_data_dir / file_name

        with open(file_name_list_dir, 'rb') as stream:
            all_rows = stream.readlines()

        # split into header and content
        if header_present:
            data_header = all_rows[:headerdata_split]
            data_body = all_rows[headerdata_split:]
        else:
            data_body = all_rows

        # extract data and convert to float datatype
        data_body_float = list()

        for index, data_body_row in enumerate(data_body):
            data_body_row_cleaned = data_body_row.decode().rstrip('\r\n').split('\t')
            data_body_row_cleaned = [float(data_pt) for data_pt in data_body_row_cleaned]
            data_body_float.append(data_body_row_cleaned)

        data_body_np = np.array(data_body_float)

        if save_npy:  # Add numpy arrays together prior to string transformation
            if first_array: # Replace lj_array w/ first instance of data_body_np
                lj_array = data_body_np
                first_array = False     # Don't do it again.
                first_numpy_array = file_name
            else:
                try:
                    # Combine later arrays to first file's array.
                    lj_array = np.concatenate((lj_array, data_body_np), axis=0)
                except ValueError: # Error caused if arrays' column dimensions don't line up (wrong data type)
                    print("Data from ", file_name, " doesn't match dimensions of '",
                          first_numpy_array, "'. Please check files.")

        # Convert data into string format for clean .csv output
        for irow_body in range(data_body_np.shape[0]):
            data_list_form = data_body_np[irow_body,:].tolist()
            for index, data in enumerate(data_list_form):
                data_list_form[index] = str(data)
            lj_mat.append(', '.join(data_list_form))

################################################################
### Print out final output and save to chosen file format(s) ###
################################################################

time_now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

if save_npy:
    output_file = lj_out_data_dir / (out_file_name + time_now)
    np.save(output_file, lj_array)
    print("File has been saved in .npy format here: ", output_file)

if save_csv:
    if header_present and keep_header: # Clean up the header info for attachment to data
        data_header_cleaned = list()

        for data_header_row in data_header:
            data_header_row_cleaned = str(data_header_row.decode().rstrip('\r\n').strip("\[").split('\t'))
            data_header_row_cleaned_again = data_header_row_cleaned.strip("\[]''")
            data_header_cleaned.append(data_header_row_cleaned_again)

        # Attach header info to lj_mat data
        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning) # Not cool. https://stackoverflow.com/questions/63097829/debugging-numpy-visibledeprecationwarning-ndarray-from-ragged-nested-sequences
        lj_mat_w_header = data_header_cleaned + lj_mat
        lj_mat_w_header_array = np.array(lj_mat_w_header)
    else:
        lj_mat_w_header = np.array(lj_mat)

    output_file = lj_out_data_dir / (out_file_name + time_now + '.csv')
    savetxt(output_file, lj_mat_w_header, delimiter=',', fmt="%s")
    print("File has been saved in .csv format here: ", output_file)
